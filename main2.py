import re
import urllib.request
import csv
import os

URL_TITLE_MAP = {
    'https://sdmda.bupt.edu.cn/szdw/js.htm': '教授',
    'https://sdmda.bupt.edu.cn/szdw/fjs.htm': '副教授',
    'https://sdmda.bupt.edu.cn/szdw/js1.htm': '讲师'
}

def create_directory(directory_name):
    #创建指定名称的目录，如果目录不存在的话
    if not os.path.exists(directory_name):
        os.mkdir(directory_name)

def fetch_html_content(url):
    #根据URL获取HTML内容
    response = urllib.request.urlopen(url)
    return response.read().decode('utf-8')

def extract_teacher_data(html_content):
    #从HTML内容中提取教师的数据
    pattern = re.compile(r'<img src="(/__local/.*?\.jpg).*?alt="">.*?<span class="name">(.*?)</span>.*?<span class="iden">(.*?)</span>', re.DOTALL)
    return pattern.findall(html_content)

def save_teacher_image(img_url, name):
    #保存教师的图片并返回图片的文件路径
    file_path = os.path.join('teacher_photos', name + '.jpg')
    urllib.request.urlretrieve('https://sdmda.bupt.edu.cn' + img_url, file_path)
    return file_path

def main():
    csv_data = [("Department", "Name", "Title", "Photo")]
    create_directory('teacher_photos')

    for url, title in URL_TITLE_MAP.items():
        html_content = fetch_html_content(url)
        teachers = extract_teacher_data(html_content)

        for teacher in teachers:
            img_url, name, department = teacher
            file_path = save_teacher_image(img_url, name)
            csv_data.append((department, name, title, file_path))

    # 保存数据到CSV文件
    with open('teacher_data.csv', 'w', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerows(csv_data)

    print("爬取完成!")

if __name__ == '__main__':
    main()