import re
import urllib.request
import csv
import os

# 定义URL和对应的职称
URL_TITLE_MAP = {
    'https://sdmda.bupt.edu.cn/szdw/js.htm': '教授',
    'https://sdmda.bupt.edu.cn/szdw/fjs.htm': '副教授',
    'https://sdmda.bupt.edu.cn/szdw/js1.htm': '讲师'
}

def create_directory(directory_name):
    #创建目录，如果不存在的话
    if not os.path.exists(directory_name):
        os.mkdir(directory_name)

def fetch_html_content(url):
    #获取指定URL的HTML内容
    try:
        response = urllib.request.urlopen(url)
        return response.read().decode('utf-8')
    except Exception as e:
        print(f"访问URL {url} 出错：{e}")
        return None

def extract_teacher_data(html_content):
   #利用正则表达爬取教师信息
    pattern = re.compile(r'<img src="(/__local/.*?\.jpg).*?alt="">.*?<span class="name">(.*?)</span>.*?<span class="iden">(.*?)</span>', re.DOTALL)
    return pattern.findall(html_content)

def save_teacher_image(img_url, name, count):
   #保存教师图片，并返回图片文件路径
    file_path = os.path.join('教师头像图片', f"{name}_{count}.jpg")
    urllib.request.urlretrieve('https://sdmda.bupt.edu.cn' + img_url, file_path)
    return file_path

def main():
    """主函数"""
    # 初始化CSV数据和图片目录
    csv_data = [("Department", "Name", "Title", "Photo")]
    create_directory('教师头像图片')

    # 遍历每一个URL来爬取数据
    for url, title in URL_TITLE_MAP.items():
        html_content = fetch_html_content(url)
        if html_content is None:
            continue

        teachers = extract_teacher_data(html_content)
        count = 0  # 用于处理同名教师的计数器

        # 保存教师信息和图片
        for teacher in teachers:
            img_url, name, department = teacher
            file_path = save_teacher_image(img_url, name, count)
            csv_data.append((department, name, title, file_path))
            count += 1

    # 将数据保存到CSV文件
    with open('教师信息.csv', 'w', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerows(csv_data)

    print("爬取完成!")

# 如果作为主脚本运行，则执行main函数
if __name__ == '__main__':
    main()