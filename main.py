import re
import urllib.request
import csv
import os

url = 'https://sdmda.bupt.edu.cn/szdw/js.htm'
response = urllib.request.urlopen(url)
html_content = response.read().decode('utf-8')

# 使用正则表达式提取需要的信息
pattern = re.compile(r'<img src="(/__local/.*?\.jpg).*?alt="">.*?<span class="name">(.*?)</span>.*?<span class="iden">(.*?)</span>', re.DOTALL)
matches = pattern.findall(html_content)

# 创建一个文件夹来保存图片
if not os.path.exists('teacher_photos'):
    os.mkdir('teacher_photos')

csv_data = [("Department", "Name", "Title", "Photo")]

for match in matches:
    img_url, name, department = match

    # 使用urlretrieve保存图片
    file_path = os.path.join('teacher_photos', name + '.jpg')
    urllib.request.urlretrieve('https://sdmda.bupt.edu.cn' + img_url, file_path)

    # 根据你的要求，我们假设所有的教师都是副教授，你可以根据实际情况进行修改
    title = "副教授"
    csv_data.append((department, name, title, file_path))

# 保存数据到CSV文件
with open('teacher_data.csv', 'w', encoding='utf-8') as f:
    writer = csv.writer(f)
    writer.writerows(csv_data)

print("爬取完成!")